from app import db

class Doctor(db.Document):
    name = db.StringField()
    phone = db.StringField()
    address = db.StringField()