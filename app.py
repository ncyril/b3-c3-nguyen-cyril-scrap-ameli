from flask import Flask, jsonify, render_template
from flask_mongoengine import MongoEngine

app = Flask(__name__)

# database configuration
app.config['MONGODB_SETTINGS'] = {
    'db': 'scrapingdb',
    'host': '172.17.0.1',
    'port': 27017,
    'username':'user',
    'password':'password'
}

# init mongo engine
db = MongoEngine()
db.init_app(app)

# instantiate Doctor model object
from models.Doctor import Doctor

# endpoint test
@app.route("/")
def home():
    return "home"
    
# scrap the ameli website and persist in mongo database et csv file as a backup 
@app.route("/scrap")
def post_doctor():
    # import logic to scrap and execute the scrapping method
    from services.scrap import get_scrap_result
    doctors = get_scrap_result()

    # persist every scrapped doctors
    for doctor in doctors:
        print(doctor)
        Doctor(name=doctor["name_firstname"], address=doctor["address"], phone=doctor["phone"]).save()

    return jsonify(message="success"), 200

# get all doctors by ordered name and render in the webpage 
@app.route("/get_doctors")
def get_doctors():
    doctors = Doctor.objects().order_by('name')
    return render_template("doctors_list.html", doctors=doctors)

# delete a doctor by his name
@app.route("/delete_doctor/<doctor_name>")
def delete_doctor_by_name(doctor_name):
    Doctor.objects(name=doctor_name).first().delete()
    return jsonify(message="success"), 200

if __name__ == "__main__":
    app.run(debug=True)