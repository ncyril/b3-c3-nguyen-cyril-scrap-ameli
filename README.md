# MONGO SCRAPPING PROJECT

1. Set environment
```bash
docker run -dti -p 80:80 --name mongo-scrapping python:3.11
```
```bash
docker run -dti -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=user -e MONGO_INITDB_ROOT_PASSWORD=password -e MONGO_INITDB_DATABASE=scrappingdb --name mongo-scrapping-db mongo:lates
```

2. Clone the projet
```bash
git clone https://gitlab.com/ncyril/mongodb-scraping.git
```

3. Install need packages

```bash
pip install -r requirements.txt
```

4. Run application
```bash
flask --app app.py run
```

5. Go on http://127.0.0.1:5000/scrap in a browser to begin the scraping, you can see the ongoing process in the terminal of the server

6. Go on http://127.0.0.1:5000/get_doctors in a browser to see the scrapped data of doctors

7. You can delete a doctor with http://127.0.0.1:5000/delete_doctor/<doctor_name>